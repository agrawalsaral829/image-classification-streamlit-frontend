import streamlit as st
import requests
import base64
import logging
logging.getLogger('streamlit').setLevel(logging.ERROR)
st.title('Butterfly Species Prediction')

# Display the default images
st.image('images.jpeg')
st.title('Classes')
butterfly_classes = [
    "Danaus Plexippus",
    "Heliconius Charitonius",
    "Heliconius Erato",
    "Junonia Coenia",
    "Lycaena Phlaeas",
    "Nymphalis Antiopa",
    "Papilio Cresphontes",
    "Pieris Rapae",
    "Vanessa Atalanta",
    "Vanessa Cardui"
]
def display_names_in_grid(names, columns=3):
    total_names = len(names)
    
    for idx in range(0, total_names, columns):
        cols = st.columns(columns)
        for col, name in zip(cols, names[idx:idx+columns]):
            col.write(name)
display_names_in_grid(butterfly_classes, columns=3)

# File uploader for user-uploaded images
uploaded_file = st.file_uploader("Upload a Butterfly Image", type=["jpg", "jpeg", "png"])

# Update this URL to point to your deployed Flask API
url = 'https://image-nsga.onrender.com/predict'

if st.button('Predict'):
    if uploaded_file is not None:
        try:
            files = {'file': uploaded_file}
            response = requests.post(url, files=files)
            
            if response.status_code != 200:
                response.raise_for_status()  # Raise HTTPError for bad responses
            
            result = response.json()
            # Display the image
            image_base64 = result['image_base64']
            image_data = base64.b64decode(image_base64)
            st.image(image_data)
            st.header(result['prediction_text'])
        
        except requests.exceptions.HTTPError as http_err:
            st.error(f'HTTP error occurred: {http_err}')
        except requests.exceptions.ConnectionError as conn_err:
            st.error(f'Connection error occurred: {conn_err}')
        except requests.exceptions.Timeout as timeout_err:
            st.error(f'Timeout error occurred: {timeout_err}')
        except requests.exceptions.RequestException as req_err:
            st.error(f'An error occurred: {req_err}')
    else:
        st.error("Please upload an image.")

st.cache_data.clear()

# Clearing all cached resource functions
st.cache_resource.clear()        
